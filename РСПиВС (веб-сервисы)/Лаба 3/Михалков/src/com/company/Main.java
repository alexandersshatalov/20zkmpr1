package com.company;

import java.util.Arrays;
import java.util.Random;

public class Main {
    /*Даны матрицы С и D размерностью 3 на 3 и заполненные случайными числами в диапазоне от 0 до 99.
Выполните по отдельности сначала сложение, потом умножения матриц друг на друга.
Выведете исходные матрицы и результат вычислений на консоль.*/
    public static void main(String[] args) {
        int[][] c = Randomize();
        int[][] d = Randomize();

        int[][] sum = Add(c, d);
        int[][] mul = Multiple(c, d);

        System.out.println("Матрица C:");
        Show(c);
        System.out.println("Матрица D:");
        Show(d);
        System.out.println("С + D =");
        Show(sum);
        System.out.println("C x D");
        Show(mul);

    }
    public static int[][] Randomize() {
        int[][] array = new int [3][3];
        final Random r = new Random();
        for (int i = 0; i < array.length; i++) { //array.length выведет изначально длину первого измерения. Оба измерения равны по задаче.
            for (int j = 0; j < array.length; j++) {
                array[i][j] = r.nextInt(100); //диапазон [0; 100)
            }
        }
        return array;
    }

    public static int[][] Add(int[][] a, int[][] b) {
        int[][] c = new int[3][3];
        for (int i = 0; i < c.length; i++){
            for (int j = 0; j < c.length; j++) {
                c[i][j] = a[i][j] + b[i][j];
            }
        }
        return c;
    }

    public static int[][] Multiple(int[][] a, int[][] b) {
        int[][] c = new int[3][3];
        for (int i = 0; i < c.length; i++){
            int [] a1 = a[i]; // фиксированное i, плавающее j
            for (int j = 0; j < c.length; j++) {
                int[] b1 = new int[3];//фиксированная j, но плавающая i, делаем дубликат i1
                for (int i1 = 0; i1 < c.length; i1++) {
                    b1[i1] = b[i1][j];
                }
                c[i][j] = GetResult(a1, b1);
            }
        }
        return c;
    }

    public static int GetResult(int[] a, int[] b) {
        int[] c = new int[3];
        for (int i = 0; i < c.length; i++) {
            c[i] = a[i] * b[i];
        }
        return Arrays.stream(c).sum();
    }

    public static void Show(int[][] a) {
        for (int i = 0; i < a.length; i++){
            for(int j=0; j < a[i].length; j++){
                System.out.printf("%02d\t", a[i][j]);
            }
            System.out.println();
        }
    }
}
