package com.company;
import java.util.ArrayList;
public class Main {

    public static void main(String[] args) {
	    ArrayList<String> numbers = new ArrayList<String>();
	    for (int i = 1; i < 100000; i++) {
	        numbers.add(String.format("%05d", i));
        }
	    int before = numbers.size();
	    numbers.removeIf(x -> x.contains("13") || x.contains("4"));
	    int after = numbers.size();
	    System.out.printf("Всего необходимо исключить %d чисел\n", before - after);
    }
}
