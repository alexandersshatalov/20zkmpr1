package com.company;

public interface IFigure {
    double getSquare();
}
