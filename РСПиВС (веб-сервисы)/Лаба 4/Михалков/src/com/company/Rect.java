package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class Rect {
    private int xFrom;
    private int xTo;
    private int yFrom;
    private int yTo;

    private int H;
    private int W;

    Rect(int x, int y, int xLen, int yLen) {
        xFrom = x;
        xTo = x + xLen;
        yFrom = y;
        yTo = y + yLen;

        W = xLen;
        H = yLen;
    }
    Rect() {
        this(0, 0, 0, 0);
    }

    Rect(Point p1, Point p2){
        if (p1.getX() != p2.getX() && p1.getY() != p2.getY()){
            //this(p1.getX(), p1.getY(), p2.getX()-p1.getX(), p2.getY()-p1.getY());
            xFrom = p1.getX();
            xTo = p1.getX() + p2.getX()-p1.getX();
            yFrom = p1.getY();
            yTo = p1.getY() + p2.getY()-p1.getY();

            W = p2.getX()-p1.getX();
            H = p2.getY()-p1.getY();
        } else { //пренебрежём наличием линии, по логике Рект - не Рект.
            xFrom = 0;
            xTo = 0;
            yFrom = 0;
            yTo = 0;

            W = 0;
            H = 0;
        }
    }

    public int getxFrom() {
        return xFrom;
    }

    public void setxFrom(int xFrom) {
        int old = this.xFrom;
        this.xFrom = xFrom;
        this.xTo += (xFrom - old);
    }

    public int getxTo() {
        return xTo;
    }

    public void setxTo(int xTo) {
        int old = this.xTo;
        this.xTo = xTo;
        this.xFrom += (xTo - old);
    }

    public int getyFrom() {
        return yFrom;
    }

    public void setyFrom(int yFrom) {
        int old = this.yFrom;
        this.yFrom = yFrom;
        this.yTo -= (yFrom - old);
    }

    public int getyTo() {
        return yTo;
    }

    public void setyTo(int yTo) {
        int old = this.yTo;
        this.yTo = yTo;
        this.yFrom -= (yTo - old);
    }

    public int getH() {
        return H;
    }

    public void setH(int h) {
        H = h;
        yTo = yFrom + h;
    }

    public int getW() {
        return W;
    }

    public void setW(int w) {
        W = w;
        xTo = xFrom + w;
    }

    public Point[] GetPts() {
        String text = "Полученный прямоугольник имеет координаты:\n";
        text += String.format("A: (%d; %d)\n", xFrom, yFrom);
        text += String.format("B: (%d; %d)\n", xFrom, yTo);
        text += String.format("C: (%d; %d)\n", xTo, yTo);
        text += String.format("D: (%d; %d)\n", xTo, yFrom);
        text += String.format("AB = CD = %d\nBC = AD = %d\n", Math.abs(H), Math.abs(W));
        System.out.println(text);
        return new Point[] {new Point(xFrom, yFrom), new Point(xFrom, yTo), new Point(xTo, yTo), new Point(xTo, yFrom)};
    }

    public Rect getDoubleRect() {
        if (H < W) {
            return new Rect(xFrom, yFrom, W, H*2);
        } else {
            return new Rect(xFrom, yFrom, W*2, H);
        }
    }

    public Rect getIntersect(Rect r){
        Point[] ourPts = GetPts();
        Point[] theirPts = r.GetPts();
        int counter = 0;
        for (int i = 0; i < ourPts.length; i++) {
            for (int j = 0; j < theirPts.length; j++) {
                if (ourPts[i] == theirPts[j]){
                    counter++;
                    break;
                }
            }
        }
        if (counter == ourPts.length) {
            return this;
        }
        //int counterInRect = 0;
        ArrayList<Point> ptsInRect = new ArrayList<Point>();
        for (Point p : theirPts) {
            if (isInRect(p)) {
                ptsInRect.add(p);
            }
        }
        if (ptsInRect.size() == theirPts.length) {
            return r;
        }
        else {
            int oldsize = ptsInRect.size();
            ArrayList<Point> allNewPts = new ArrayList<Point>();
            for (Point pt : ptsInRect)
                allNewPts.add(pt);

            ptsInRect.removeIf(pt -> isInLine(pt)); // в ректе, но не на линиях
            if (oldsize == 0) {
                return new Rect();
            }
            else {
                if (oldsize == 2) {
                    if (allNewPts.get(0).getX() == allNewPts.get(1).getX()){ //равенство точек по иксам
                        if (r.getxFrom() < xFrom) {
                            r.setxFrom(xFrom);
                            r.setW(allNewPts.get(0).getX());
                            return r; // сдвиг + обрезка
                        }
                        else if (r.getxTo() > xTo) {
                            r.setW(xTo);
                            return r; //сразу обрезка
                        }
                    }
                    else if (allNewPts.get(0).getY() == allNewPts.get(1).getY()) {
                        if (r.getyFrom() < yFrom) {
                            r.setyFrom(yFrom);
                            r.setH(allNewPts.get(0).getY());
                            return r; // сдвиг + обрезка
                        }
                        else if (r.getyTo() > yTo) {
                            r.setH(yTo);
                            return r; //сразу обрезка
                        }
                    }
                }
                else if (oldsize == 1) {
                    if (r.getxFrom() < xFrom) {
                        if (r.getyFrom() < yFrom) {
                            return new Rect (new Point(xFrom, yFrom), allNewPts.get(0));
                        } else {
                            return new Rect (new Point(xFrom, yTo), allNewPts.get(0));
                        }
                    }
                    else if (r.getxTo() > xTo) {
                        if (r.getyFrom() < yFrom) {
                            return new Rect (new Point(xTo, yFrom), allNewPts.get(0));
                        } else {
                            return new Rect (new Point(xTo, yTo), allNewPts.get(0));
                        }
                    }
                }
            }
        }
        return new Rect();
    }

    public boolean isInRect(Point p) {
        return xFrom <= p.getX() && p.getX() <= xTo && yFrom <= p.getY() && p.getY() <= yTo;
    }
    public boolean isInLine(Point p) {
        return p.getX() == xFrom || p.getX() == xTo || p.getY() == yFrom || p.getY() == yTo;
    }
}
